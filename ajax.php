<?php

  $data = file_get_contents("php://input");
  $req = json_decode($data);
  $type = $req->type;
  
  
  if (file_exists('czasopisma.xml')) {
  
    $xml = simplexml_load_file('czasopisma.xml');
     
    switch ($type) {
    	case 'onload':
    		$query = "//zmienne/*";
            $entries = $xml->xpath($query);
            echo(json_encode($entries));
    		break;
        case 'rok':
          	$value = $req->value;
            $query = "//lata/".$value;
            $entries = $xml->xpath($query);
            echo(json_encode($entries));

            break;

        case 'content':
        	$value = $req->value;
            $tmp = explode("|", $value);
       
            //echo $tmp[0].' - '.$tmp[1];
            if($tmp[0] == 'wszystkie'){
                $query = "//".$tmp[1]."/*[@rok]";
                $entries = $xml->xpath($query);
                echo(json_encode($entries));
            }else{
                $query = "//".$tmp[1]."/*[@rok='".$tmp[0]."']";
                $entries = $xml->xpath($query);
                echo(json_encode($entries));
            }

            
            break;
            
    }

  }

  
  
?>